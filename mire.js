const url = 'https://rickandmortyapi.com/api/character';

    fetch(url)
        .then(res => res.json())
        .then(data => {
            const rawData = data.results;
            return rawData.map(character => {
                //all needed data is listed below as an entity
                let created = character.created;
                let species = character.species;
                let img = character.image;
                let episodes = character.episode;
                let name = character.name;
                let location = character.location;
                //create element
                //append element
                const container = document.getElementById('container')
                const item =document.createElement('div')
                item.classList.add("post")

                //creating <div class='image>'
                const containerForImage = document.createElement('div')
                containerForImage.classList.add('post-img')

                const itemImg =document.createElement('img')
                itemImg.src = img

                //creating <div class='post-info'> for text under image
                const postInfo = document.createElement('div')
                postInfo.classList.add('post-info')

                const itemName= document.createElement('ul')
                itemName.innerHTML = name


                const itemCreated = document.createElement('li');
                itemCreated.innerHTML = created

                const itemSpecies = document.createElement('li');
                itemSpecies.innerHTML = species;

                const itemEpisodes = document.createElement('li');
                itemEpisodes.innerHTML = episodes;

                const itemLocationName = document.createElement('span')
                const itemLocation = document.createElement('li')
                const space = document.createElement('span')


                itemLocation.innerHTML =  location.url
                space.innerText =" "
                itemLocationName.innerHTML = location.name

                container.appendChild(item)

                item.appendChild(containerForImage)//add container for image in <div class='post'>
                containerForImage.appendChild(itemImg)
                item.appendChild(postInfo)

                postInfo.appendChild(itemName)
                postInfo.appendChild(itemCreated)
                postInfo.appendChild(itemSpecies)
                postInfo.appendChild(itemEpisodes)
                postInfo.appendChild(itemLocation)
                itemLocation.appendChild(space)
                itemLocation.appendChild(itemLocationName)

            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
